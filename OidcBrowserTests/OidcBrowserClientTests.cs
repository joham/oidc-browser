﻿using IdentityModel.OidcClient.Browser;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OidcBrowser;

namespace OidcBrowserTests
{
    [TestClass]
    public class OidcBrowserClientTests
    {
        private OidcBrowserClient oidcBrowserClient;

        [TestInitialize]
        public void Initialize()
        {
            oidcBrowserClient = new OidcBrowserClient();
        }

        [TestMethod]
        public void TestMethod1()
        {
            BrowserOptions options = new BrowserOptions("https://start.url", "https://end.url");

            oidcBrowserClient.InvokeAsync(options);
        }
    }
}
